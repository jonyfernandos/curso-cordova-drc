var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        dialogsApp.init();   
    }
};

app.initialize();

var dialogsApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");
        this.bindElements();
    },
    bindElements : function(){
        _id('alerta').onclick  = function(){dialogsApp.alert();   }
        _id('confirm').onclick = function(){dialogsApp.confirm(); }
        _id('prompt').onclick  = function(){dialogsApp.prompt();  }
    },
    alert   : function(text, callback, buttons){
        navigator.notification.alert(
            text || "Isto é um alert nativo", //Mensagens
            callback || null, //Calbeack
            "Truma da DRC", //Titulo
            buttons || "LEgal! :)" //Botao
        );
    },
    confirm : function(text, callback, buttons){
        navigator.notification.confirm(
            text || "Voce vai clicar não?", //MGS
            callback || function(e){console.info(e);}, //Callback
            "Turma DRC",
            buttons || ["Sim", "Desisto", "Não"] //Botões
        );
    },
    prompt  : function(text, buttons, callback, defaultText){
        navigator.notification.prompt(
            text || "Qual é o seu nome? ",
            callback || onPrompt,
            "Turma da DRC",
            buttons || ["OK", "Cancelar"], //Botoes
            //"Texto Padrão" //Texto default(evite usar para não aparecer as ferramentas de copy and paste do device)
            defaultText || null
        );
    }
}

function dialog(type, text, buttons, callback, defaultText) {
    if (!type) return;
    dialogsApp[type](text, buttons, callback, defaultText);
}

function onPrompt(data) {
    alert("Você escreveu" + data.input1 + "e apertou o botão número" + data.buttonIndex);
}

function _id(id) {
    return document.getElementById(id);
}
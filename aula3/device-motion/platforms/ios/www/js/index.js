var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        deviceMotionApp.init();   
    }
};

app.initialize();

var deviceMotionApp = {
    var oldX : 0;
    var oldY : 0;

    init : function(){
        //alert("A comunicação com as APIS do device está concluída");
        var options = {
            frequency : 16.66 // 1 s por 60 frames
        }
        navigator.accelerometer.whatchAcceleration(this.onSucces, this.onError, options);
    },
    onSucces : function(data){
        console.log(data.x, data.y, data.z);
        _bE("x").innerText = data.x;
        _bE("y").innerText = data.y;
        _bE("z").innerText = data.z;

        if ( (oldX - data.x < 1) || (oldY - data.y < -1) ) {
            oldX = data.x;
            oldY = data.y;
        } else {
            return
        };

        data.x = data.x * -1;
        data.y = data.y * -1;
        var newX = (x + 10) * 5;
        var newY = (y + 10) * 5;
        // document.getElementsByClassName('boy')[0].setAttribute('style', "translate3d(" + newX + "%" + newY + ,"%, 0)');
        document.getElementsByClassName('boy')[0].style.top = newX + '%';
        document.getElementsByClassName('boy')[0].style.top = newY + '%';
    },
    onError : function(error){
        console.log(error);
    }
}

function _bE(id) {
    return document.getElementById(id);
}
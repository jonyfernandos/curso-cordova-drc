var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        geolocationApp.init();   
    }
};

app.initialize();

var geolocationApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");
        navigator.geolocation.getCurrentPosition(geolocationApp.initMap);
    },
    initMap : function(position){
        var lat = position.coords.latitude,
            lon = position.coords.longitude;
        document.getElementById('latitude-in').innerText = lat;
        document.getElementById('longitude-in').innerText = lon;

        googleMaps.initialize(lat, lon)
    }
}
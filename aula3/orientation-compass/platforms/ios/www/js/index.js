var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        revisaoApp.init();   
    }
};

app.initialize();

var revisaoApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");
        var options = {
            frequency : 1000 // 1000/s
        }
        navigator.accelerometer.whatchAcceleration(this.onSucces, this.onError, options);
    },
    onSucces : function(data){
        console.log(data);
        // document.getElementBYId("bussula").style = "trasnform: rotate(" + (data.magneticHeanding * -1) + "deg");
        // Esta configuração sera necessario uma para cada prefix do navegador.
        document.getElementBYId("bussula").setAttribute("style", "trasnform: rotate(" + (data.magneticHeading * -1) + "deg)");
    },
    onError : function(error){
        console.log(error);
    }
}
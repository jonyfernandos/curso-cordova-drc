var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        deviceInfoApp.init();   
    }
};

app.initialize();

var deviceInfoApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");
        this.bindElements();
    },
    bindElements : function(){
        document.getElementById('device-info').onclick = this.showDeviceInfo;
    },
    showDeviceInfo : function(){
        console.log(device); //device fica disponivel pelo plugin
        alert(
            "Platform: "                + device.manufacturer + "\n" +
            "Versao do"                 + device.platform     + ": " + device.version + "\n" +
            "Versao do Cordova: "       + device.cordova      +
            "Identificador Iniversal: " + device.uuid         + "\n" +
            "Modelo: "                  + device.model
        );
    }
}
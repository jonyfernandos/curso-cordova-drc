var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        revisaoApp.init();   
    }
};

app.initialize();

var revisaoApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");

        this.bindElements();
    },
    bindElements : function() {
        document.getElementById('open-camera').onclick = this.openCamera;
    },
    openCamera : function() {
        navigator.camera.getPicture(
            cameraApp.onPictureOk, //Callback de Sucesso
            cameraApp.onError, //Callback de error
            {
                quality: 50,
                //destinationType: Camera.destinationType.DATA_URL //jpeg;base64
                destinationType: Camera.DestinationType.FILE_URI //para url
            }, //Objeto de configuração
        );
    },
    onPictureOk : function(data){
        var img = new Image();
        //img.src = 'data:image/jpeg;base64,' + data;
        img.src = data;
        document.getElementsByTagName('body')[0].appendChild(img);
    },
    onError : function(error){
        console.error(error);
    }
}
var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        detectPlatform.init();   
    }
};

app.initialize();

var detectPlatform = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");

        vat userAgent = navigator.userAgent;

        if (userAgent.match(/(Iphone|Ipad|Ipod)/)) {
            alert('This device runs IOS');
        } else if (userAgent.match(/(Android)/)) {
            alert('This device runs Android');
        } else if (userAgent.match(/(BB10)/)) {
            alert('This device runs BlackBarry 10');
        } else {
            alert('Unknown platform' + userAgent);            
        };
    }
}
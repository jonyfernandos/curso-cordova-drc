var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        revisaoApp.init();   
    }
};

app.initialize();

var revisaoApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");

        this.bindElements();
    },
    bindElements : function() {
        window.addEventListener('batterycritical', this.onBatteryCritical, null);
        window.addEventListener('batterylow', this.onBatteryLow, null);
        window.addEventListener('batterystatus', this.onBatteryStatus, null);
    },
    onBatteryCritical : function(info) {
        alert('Battery Level is critical: ' + info.level + '%');
    },
    onBatteryLow : function(info) {
        alert('Battery Level is low: ' + info.level + '%');
    },
    onBatteryStatus : function(info) {
        alert('Battery Level: ' + info.level + '% e' + (!info.isPlugged ? 'não':'') +
            'está plugado no celular');
    },
}
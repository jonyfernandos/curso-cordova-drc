var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        contactsApp.init();   
    }
};

app.initialize();

var contactsApp = {
    init : function(){
        //alert("A comunicação com as APIS do device está concluída");

        this.bindElements();
    },
    bindElements : function(){
        document.getElementById('search').onsubmit = this.getContacts;
    },
    getContacts : function(e){
        e ? e.preventDefault() : false; // Se é 'e' é e.preventDefault() senão false.
        var options = new ContactFindoptions();
        options.filter = document.getElementById('search-text').value;
        options.multiple = true;
        navigator.contacts.find(['name', 'displayName'], contactsApp.onContactOk, contactsApp.error, options)
    },
    onContactOk : function(contacts){
        console.log(contacts);
        var lgt = contacts.length;
        var html = '';
        for (var i = 0; i < lgt; i++) {
            html += '<li> Id: ' + contacts[i].id + '| Nome: ' + contacts[i].displayName || 'Sem nome de Display') + '</li>';
        }
        
        document.getElementById('results-in').innerHTML = html;
        contactsApp.toggleResults(lgt);      
        
    },
    toggleResults : function(lgt){
            document.getElementById('results').className = !lgt ? 'hidden' : '';
            document.getElementById('nothing-found').className = lgt ? 'hidden' : '';
        }
    error : function(error){
        console.log(error);
    }
}
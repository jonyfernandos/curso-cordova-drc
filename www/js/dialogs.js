var dialogsApp = {
    alert : function(text, buttons, callback){
        navigator.notification.alert(
            text     || "Isto é um alerta nativo", // Mensagem
            callback || null, // Callback (funcao)
            "Turma da DRC", // Título
            buttons  || "Botão padrão" // Botão
        );
    },
    confirm : function(text, buttons, callback){
        navigator.notification.confirm(
            text     || "Janela de confirmação padrão?", // Mensagem
            callback || function(e){console.info(e);}, // Callback
            "Turma da DRC",
            buttons  || ["Sim", "Não"] // Botões
        );
    },
    prompt : function(text, buttons, callback, defaultText){
        navigator.notification.prompt(
            text        || "Qual é o seu nome?", // Mensagem (neste caso uma pergunta)
            callback    || onPrompt, // Callback
            "Turma da DRC", // Título
            buttons     || ["Ok", "Cancelar"], // Botões
            defaultText || null // Texto default (evite usar para não aparecer as ferramentas de copy and paste do device)
        )
    }
}

function dialog(type, text, buttons, callback, defaultText){
    if(!type) return;
    dialogsApp[type](text, buttons, callback, defaultText);
}

function _id(id){
    return document.getElementById(id);
}

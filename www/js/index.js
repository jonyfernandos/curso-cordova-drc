var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
        revisaoApp.init();   
    }
};

app.initialize();

var revisaoApp = {
    init : function(){
        alert("A comunicação com as APIS do device está concluída");
    }
}



// this == window;

// var jony = {
//     init : function(){
//         this == jony;
//     },
//     callback : function(e){
//         this == e.target; // geralmente
//     }
// }

// document.addEventListener("click", jony.callback, false);